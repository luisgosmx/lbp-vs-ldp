import cv2
import numpy as np
from matplotlib import pyplot as plt
import time

#condition to assign 0 or 1 depending on the received value
def get_pixel(img, center, x, y):
    new_value = 0
    try:
        if img[x][y] >= center:
            new_value = 1
    except:
        pass
    return new_value

#function that performs the extraction of the coordinates
def lbp_calculated_pixel(img, x, y):
    center = img[x][y]
    val_ar = []

    val_ar.append(get_pixel(img, center, x - 1, y - 1))
    val_ar.append(get_pixel(img, center, x - 1, y))
    val_ar.append(get_pixel(img, center, x - 1, y + 1))
    val_ar.append(get_pixel(img, center, x, y + 1))
    val_ar.append(get_pixel(img, center, x + 1, y + 1))
    val_ar.append(get_pixel(img, center, x + 1, y))
    val_ar.append(get_pixel(img, center, x + 1, y - 1))
    val_ar.append(get_pixel(img, center, x, y - 1))

    power_val = [1, 2, 4, 8, 16, 32, 64, 128]
    val = 0

    #assign binary value
    for i in range(len(val_ar)):
        val += val_ar[i] * power_val[i]
    return val

#start the time count
start = time.time()
path = '' # image path
img_bgr = cv2.imread(path,  cv2.COLOR_BGR2RGB) #read the image and get its colors in rgb

height, width, _ = img_bgr.shape # extract size

img_gray = cv2.cvtColor(img_bgr, cv2.COLOR_RGB2GRAY)  #converts pixels to gray

img_lbp = np.zeros((height, width), np.float32) #create an array of the same size and then assign the values

#send each  matrix
for i in range(0, height):
    for j in range(0, width):
        img_lbp[i, j] = lbp_calculated_pixel(img_gray, i, j)


# show results
plt.imshow(img_bgr)
plt.show()

plt.imshow(img_lbp, cmap="gray")
plt.show()

print("LBP Program is finished")
end = time.time()
print('execution time ', end - start)
